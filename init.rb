# Copyright (C) 2021  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Redmine::Plugin.register :public_project_permission do
  name "Public project permission plugin"
  author "ClearCode Inc."
  description "This is a Redmine plugin to control public project's permission by user's custom field value"
  version "1.0.2"
  url "https://gitlab.com/clear-code/redmine-plugin-public-project-permission"
  author_url "https://gitlab.com/clear-code"
  directory __dir__
  settings partial: "settings/public_project_permission"
end

Rails.configuration.to_prepare do
  require_dependency "public_project_permission/custom_field_checker"

  require_dependency "public_project_permission/project_allowable"
  Project.singleton_class.prepend(PublicProjectPermission::ProjectAllowable)
  require_dependency "public_project_permission/user_allowable"
  User.prepend(PublicProjectPermission::UserAllowable)

  require_dependency "public_project_permission/settings"
  Setting.singleton_class.prepend(PublicProjectPermission::SettingsObjectize)
end
