# Copyright (C) 2021  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

module PublicProjectPermission
  class CustomFieldChecker
    def initialize(true_value, false_value)
      @true_value = true_value
      @false_value = false_value

      settings = Setting.plugin_public_project_permission
      name = settings.custom_field_name
      if name
        @field = UserCustomField
                   .where(name: name,
                          field_format: "bool")
                   .first
      else
        @field = nil
      end
    end

    def need_check?
      not @field.nil?
    end

    def check(role, user)
      case role
      when Role.non_member
        value = user.custom_values.where(custom_field: @field).first
        if value&.true?
          block_given? ? yield(role, user) : @true_value
        else
          @false_value
        end
      when Role.anonymous
        @false_value
      else
        block_given? ? yield(role, user) : @true_value
      end
    end
  end
end
