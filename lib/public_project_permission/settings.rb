module PublicProjectPermission
  module SettingsObjectize
    def plugin_public_project_permission
      Settings.new(super)
    end
  end

  class Settings
    def initialize(raw)
      @raw = raw || {}
    end

    def custom_field_name
      @raw["custom_field_name"].presence
    end
  end
end
